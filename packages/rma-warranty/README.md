### Web frontend starter with ionic and angular

commands for testing

```sh
# Format code with prettier
yarn format

# Fix lint with tslint
yarn lint -- --fix

# Angular Unit Tests (karma)
yarn test -- --watch false

# Angular e2e Tests (Protractor)
yarn e2e
```
