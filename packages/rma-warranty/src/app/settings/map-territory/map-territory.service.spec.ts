import { TestBed } from '@angular/core/testing';

import { MapTerritoryService } from './map-territory.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { StorageService } from '../../api/storage/storage.service';

describe('MapTerritoryService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: StorageService,
          useValue: {
            getItem: (...args) => Promise.resolve(''),
          } as StorageService,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: MapTerritoryService = TestBed.get(MapTerritoryService);
    expect(service).toBeTruthy();
  });
});
