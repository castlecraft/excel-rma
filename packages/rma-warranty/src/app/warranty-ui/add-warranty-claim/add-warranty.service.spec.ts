import { TestBed } from '@angular/core/testing';

import { AddWarrantyService } from './add-warranty.service';
import { StorageService } from '../../api/storage/storage.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AddWarrantyService', () => {
  let service: AddWarrantyService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: StorageService,
          useValue: {
            getItem: (...args) => Promise.resolve(''),
          } as StorageService,
        },
      ],
    });
    service = TestBed.inject(AddWarrantyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
