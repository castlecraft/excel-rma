import { TestBed } from '@angular/core/testing';

import { ViewWarrantyService } from './view-warranty.service';
import { StorageService } from '../../api/storage/storage.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ViewWarrantyService', () => {
  let service: ViewWarrantyService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: StorageService,
          useValue: {
            getItem: (...args) => Promise.resolve(''),
          } as StorageService,
        },
      ],
    });
    service = TestBed.inject(ViewWarrantyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
