import { TestBed } from '@angular/core/testing';

import { StatusHistoryService } from './status-history.service';
import { StorageService } from '../../../api/storage/storage.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('StatusHistoryService', () => {
  let service: StatusHistoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: StorageService,
          useValue: {
            getItem: (...args) => Promise.resolve(''),
          } as StorageService,
        },
      ],
    });
    service = TestBed.inject(StatusHistoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
