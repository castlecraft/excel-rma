import { TestBed } from '@angular/core/testing';
import { StockEntryService } from './stock-entry.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { StorageService } from '../../../../../api/storage/storage.service';

describe('StockEntryService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: StorageService,
          useValue: {
            getItem: (...args) => Promise.resolve(''),
          } as StorageService,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: StockEntryService = TestBed.get(StockEntryService);
    expect(service).toBeTruthy();
  });
});
