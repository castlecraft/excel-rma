export const ACCEPT = 'Accept';
export const APPLICATION_JSON = 'application/json';
export const ASSIGN_SERIAL_DIALOG_QTY = 2;

export const CLOSE = 'Close';
export const CONTENT_TYPE = 'Content-Type';
export const CSV_FILE_TYPE = ' serials.csv';

export const DELIVERY_NOTE = 'delivery_note';
export const DELIVERY_TOKEN = 'Delivery Token';
export const DRAFT = 'Draft';
export const DURATION = 3000;

export const EXCEL_WARRANTY_PRINT = 'Excel Warranty Print';
export const EXCEL_SALES_MANAGER = 'Excel Sales Manager';
export const EXCEL_SALES_USER = 'Excel Sales User';
export const EXCEL_STOCK_PRINT = 'Excel Stock Print';

export const JSON_BODY_MAX_SIZE = 8000;

export const MATERIAL_TRANSFER = 'Material Transfer';

export const NEGATIVE_STOCK_ERROR = 'NegativeStockError';
export const NON_SERIAL_ITEM = 'Non Serial Item';

export const PURCHASE_USER = 'Purchase User';
export const PURCHASE_RECEIPT = 'purchase_receipt';

export const REJECTED = 'Rejected';

export const SHORT_DURATION = 5000;
export const SYSTEM_MANAGER = 'System Manager';
export const SERVICE_TOKEN = 'Service Token';
export const STOCK_AVAILABILITY_CSV_FILE = 'stock_availability.csv';
export const SERIAL_QUANTITY_CSV_FILE = 'serial_quantity.csv';
export const STOCK_LEDGER_CSV_FILE = 'ledger_report.csv';
export const SERVICE_INVOICE_CSV_FILE = 'service-invoice-list.csv';

export const TERRITORY = 'territory';

export const UPDATE_SUCCESSFUL = 'Update Successful';
export const USER_ROLE = 'user_roles';
export const UPDATE_ERROR = 'Update Error';

export const WAREHOUSES = 'warehouses';
export const WARRANTY_CLAIMS_CSV_FILE = 'warranty-claim-list.csv';

export const CURRENT_STATUS_VERDICT = {
  RECEIVED_FROM_BRANCH: 'Received from Branch',
  WORK_IN_PROGRESS: 'Work in Progress',
  SENT_TO_ENG_DEPT: 'Sent to Eng. Dept',
  SENT_TO_REPAIR_DEPT: 'Sent to Repair Dept',
  TRANSFERRED: 'Transferred',
  SOLVED: 'Solved - Repairing done',
  TO_REPLACE: 'Unsolved - To Replace',
  UNSOLVED: 'Unsolved - Return to Owner',
  DELIVER_TO_CUSTOMER: 'Deliver to Customer',
};
export const DELIVERY_STATUS = {
  REPAIRED: 'Repaired',
  REPLACED: 'Replaced',
  UPGRADED: 'Upgraded',
  REJECTED: 'Rejected',
};
export const DELIVERED_SERIALS_DATE_FIELDS = ['purchasedOn', 'soldOn'];
export const DELIVERED_SERIALS_BY = {
  sales_invoice_name: 'sales_invoice_name',
  purchase_invoice_name: 'purchase_invoice_name',
  stock_entry_uuid: 'stock_entry_uuid',
};
export const INVOICE_STATUS = {
  DRAFT: 'Draft',
  COMPLETED: 'Completed',
  TO_DELIVER: 'To Deliver',
  CANCELED: 'Canceled',
  REJECTED: 'Rejected',
  SUBMITTED: 'Submitted',
  ALL: 'All',
};
export const INVOICE_DELIVERY_STATUS = {
  DELIVERED_TO_CUSTOMER: 'Delivered to Customer',
  KEPT_IN_WAREHOUSE: 'Kept in Warehouse',
};
export const ITEM_COLUMN = {
  SERIAL_NO: 'serial_no',
  ITEM: 'item',
  ITEM_NAME: 'item_name',
  ITEM_CODE: 'item_code',
  QUANTITY: 'quantity',
  RATE: 'rate',
  WAREHOUSE: 'warehouse',
  STOCK_ENTRY_ITEM_TYPE: 'stock_entry_type',
};
export const MATERIAL_TRANSFER_DISPLAYED_COLUMNS = [
  's_warehouse',
  't_warehouse',
  'item_name',
  'qty',
  'serial_no',
  'delete',
];
export const SERVICE_INVOICE_STATUS = {
  SUBMITTED: 'Submitted',
  PAID: 'Paid',
  UNPAID: 'Unpaid',
  ALL: 'All',
};
export const STOCK_TRANSFER_STATUS = {
  delivered: 'Delivered',
  returned: 'Returned',
  in_transit: 'In Transit',
  draft: 'Draft',
  all: 'All',
  reseted: 'Reseted',
};
export const STOCK_ENTRY_ITEM_TYPE = {
  RETURNED: 'Returned',
  DELIVERED: 'Delivered',
};
export const SALES_INVOICE_STATUS = {
  CANCELED: 'Canceled',
  REJECTED: 'Rejected',
};
export const STOCK_ENTRY_STATUS = {
  REPLACE: 'Replace',
  UPGRADE: 'Upgrade',
  SPARE_PARTS: 'Spare Parts',
};
export const STOCK_ENTRY_TYPE = {
  MATERIAL_TRANSFER: 'Material Transfer',
  MATERIAL_RECEIPT: 'Material Receipt',
  MATERIAL_ISSUE: 'Material Issue',
  RnD_PRODUCTS: 'R&D Products',
};
export const SUBMIT_STATUS = {
  SUBMITTED: 'Submitted',
  NOT_SUBMITTED: 'Not Submitted',
  CANCELED: 'Canceled',
  NOT_AVAILABLE: 'Status not available',
};
export const SERIAL_DOWNLOAD_HEADERS = [
  'serial_no',
  'item_code',
  'item_name',
  'warehouse',
];
export const SERIAL_QUANTITY_DOWNLOAD_HEADERS = [
  'item_name',
  'item_code',
  'brand',
  'warehouse',
  'total',
];
export const STOCK_AVAILABILITY_DOWNLOAD_HEADERS = [
  'item_name',
  'item_code',
  'item_group',
  'brand',
  'warehouse',
  'stockAvailability',
];
export const STOCK_LEDGER_REPORT_HEADERS = [
  'posting_date',
  'item_name',
  'item_code',
  'item_group',
  'voucher_no',
  'voucher_type',
  'brand',
  'warehouse',
  'stock_uom',
  'actual_qty',
  'balance_qty',
  'incoming_rate',
  'outgoing_rate',
  'valuation_rate',
  'balance_value',
];
export const SERVICE_INVOICE_DOWNLOAD_HEADERS = [
  'customer_name',
  'date',
  'total',
];
export const WARRANTY_CLAIMS_DOWNLOAD_HEADERS = [
  'claim_no',
  'claim_type',
  'received_date',
  'customer_third_party',
  'customer_name',
  'third_party_name',
  'item_code',
  'claimed_serial',
  'claim_status',
  'receiving_branch',
  'delivery_branch',
  'received_by',
  'delivered_by',
  'product_brand',
  'received_serial',
  'replaced_serial',
  'problem',
  'verdict',
  'delivery_date',
  'billed_amount',
  'outstanding_amount',
  'remarks',
];
export const WARRANTY_TYPE = {
  WARRANTY: 'Warranty',
  NON_WARRANTY: 'Non Warranty',
  NON_SERIAL: 'Non Serial Warranty',
  THIRD_PARTY: 'Third Party Warranty',
};
export enum CLAIM_STATUS {
  IN_PROGRESS = 'In Progress',
  TO_DELIVER = 'To Deliver',
  DELIVERED = 'Delivered',
  UNSOLVED = 'Unsolved',
  REJECTED = 'Rejected',
  CANCELLED = 'Cancelled',
  ALL = 'All',
}
export enum CATEGORY {
  BULK = 'Bulk',
  SINGLE = 'Single',
  PART = 'Part',
}
export enum DATE_TYPE {
  RECEIVED_DATE = 'Received Date',
  DELIVERED_DATE = 'Delivery Date',
}
