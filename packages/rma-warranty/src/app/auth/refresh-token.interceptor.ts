import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
} from '@angular/common/http';
import { Observable, catchError, switchMap, throwError } from 'rxjs';
import { TokenService } from './token/token.service';
import { AUTHORIZATION, LOGGED_IN } from '../constants/storage';
import { StorageService } from '../api/storage/storage.service';

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {
  constructor(
    private readonly token: TokenService,
    private readonly storageService: StorageService,
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError(error => {
        if (
          error &&
          error?.error?._server_messages?.includes('OAuth Bearer Token') &&
          error?.status === 404
        ) {
          this.storageService.setItem(LOGGED_IN, 'false').then(() => {});
        }
        if (
          error &&
          !error?.error?._server_messages?.includes('Sales Invoice')
        ) {
          if (
            (error && error?.error?.exception?.includes('PermissionError')) ||
            (error && error?.error?.statusCode > 400) ||
            (error && request.url.includes('/api/command'))
          ) {
            return this.handleError(request, next);
          }
        }
        return throwError(() => error);
      }),
    );
  }

  private handleError(request: HttpRequest<any>, next: HttpHandler) {
    return this.token.refreshToken().pipe(
      switchMap(token => {
        return next.handle(
          request.clone({
            headers: request.headers.set(AUTHORIZATION, token),
          }),
        );
      }),
    );
  }
}
