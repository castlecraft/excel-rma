import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AssignSerialsDialog } from '../../warranty-ui/warranty/warranty.page';

@Component({
  selector: 'select-dump-headers-dialog',
  templateUrl: './select-dump-headers-dialog.html',
  styleUrls: ['./select-dump-headers-dialog.scss'],
})
export class SelectDumpHeadersDialog {
  keys = [];
  constructor(
    public dialogRef: MatDialogRef<AssignSerialsDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.keys = Object.keys(data);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  parsedTitle(title) {
    return title
      .split('_')
      .filter(element => {
        return element.charAt(0).toUpperCase() + element.slice(1);
      })
      .join(' ');
  }
}
