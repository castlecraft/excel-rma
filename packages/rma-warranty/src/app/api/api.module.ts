import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { AppCommonModule } from '../common/app-common.module';
import { FormsModule } from '@angular/forms';
import { SelectDumpHeadersDialog } from './csv-json/select-dump-headers-dialog';

@NgModule({
  declarations: [SelectDumpHeadersDialog],
  imports: [CommonModule, MaterialModule, FormsModule, AppCommonModule],
  exports: [SelectDumpHeadersDialog],
  bootstrap: [SelectDumpHeadersDialog],
})
export class ApiModule {}
