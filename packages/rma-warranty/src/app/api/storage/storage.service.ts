import { Injectable } from '@angular/core';
import { from, of, throwError, Observable, BehaviorSubject } from 'rxjs';
import {
  switchMap,
  retry,
  catchError,
  delay,
  share,
  map,
} from 'rxjs/operators';
import { Storage } from '@capacitor/storage';

export const DB_NAME = 'app_data';
export const APP_INIT = 'app_init';
export const PUT_DOC = 'put_doc';
export const GET_DOC = 'get_doc';
export const GET_ITEM = 'get_item';
export const REMOVE_ITEM = 'remove_item';
export const SET_ITEM = 'set_item';
export const SETTINGS_ID = 'settings_id';
export const CLEAR_STORAGE = 'clear_storage';
@Injectable({
  providedIn: 'root',
})
export class StorageService {
  public db = Storage;
  private onSubject = new BehaviorSubject<{ event: string; value: any }>({
    event: APP_INIT,
    value: true,
  });
  public changes = this.onSubject.asObservable().pipe(share());

  async getItem(key: string) {
    const value = await this.db.get({ key });
    this.onSubject.next({ event: GET_ITEM, value });
    return value?.value;
  }

  async setItem(key: string, value: string) {
    this.onSubject.next({ event: SET_ITEM, value: { key, value } });
    return await this.db.set({ key, value });
  }

  async removeItem(key: string) {
    this.onSubject.next({ event: REMOVE_ITEM, value: key });
    return await this.db.remove({ key });
  }

  async clear() {
    this.onSubject.next({
      event: CLEAR_STORAGE,
      value: { key: true, value: true },
    });
    return await this.db.clear();
  }

  async getItems(keys: string[]) {
    const data = {};
    for (const key of keys) {
      if (key) {
        data[keys[key]] = await this.getItem(keys[key]);
      }
    }
    return data;
  }

  getItemAsync(key: string, filter?: string): Observable<any> {
    return of({}).pipe(
      switchMap(() => {
        return from(this.getItem(key)).pipe(
          map(res => res.split(',')),
          switchMap(item => {
            if (item) {
              return filter ? of(this._filter(filter, item)) : of(item);
            }
            return throwError(() => new Error('Item Not Found')).pipe(
              delay(400),
            );
          }),
        );
      }),
      retry(9),
      catchError(() => {
        return of(undefined);
      }),
    );
  }

  _filter(value: string, state: string[]): string[] {
    const filterValue = value.toLowerCase();
    return state.filter(option => option.toLowerCase().includes(filterValue));
  }
}
