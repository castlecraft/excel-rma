import { SYSTEM_MANAGER, USER_ROLE } from '../../constants/app-string';
import { StorageService } from '../storage/storage.service';
import { from, lastValueFrom, of, throwError } from 'rxjs';
import { switchMap, catchError, map, repeat } from 'rxjs/operators';
import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import {
  PermissionRoles,
  PERMISSION_STATE,
} from '../../constants/permission-roles';

export const PermissionState = {
  create: 'create',
  read: 'read',
  update: 'update',
  delete: 'delete',
};

@Injectable({
  providedIn: 'root',
})
export class PermissionManager {
  constructor(private readonly storageService: StorageService) {}

  // module = something like "sales_invoice" , state = something like "create"

  getPermission(module: string, state: string) {
    return of({}).pipe(
      switchMap(() => {
        return from(this.storageService.getItem(USER_ROLE));
      }),
      map(roles => JSON.parse(roles)),
      switchMap((roles: string[]) => {
        if (roles && roles.length) {
          return this.validateRoles(roles, module, state);
        }
        return throwError(() => 'Retry');
      }),
      switchMap((existing_roles: string[]) => {
        if (existing_roles && existing_roles.length) {
          return of(true);
        }
        return of(false);
      }),
      repeat({ delay: 500, count: 10 }),
      catchError(() => {
        return of(false);
      }),
    );
  }

  validateRoles(user_roles: string[], module: string, state: string) {
    const roles = [];
    if (state === 'active') {
      roles.push(...this.getActiveRoles(module));
    } else {
      try {
        roles.push(SYSTEM_MANAGER);
        roles.push(...PermissionRoles[module][state]);
      } catch {
        return throwError(() => 'Module and state does not exist.');
      }
    }
    return of(_.intersection(user_roles, roles));
  }

  getActiveRoles(module: string): any[] {
    const roles = new Set();
    roles.add(SYSTEM_MANAGER);
    Object.keys(PermissionState).forEach(key => {
      if (PermissionRoles[module] && PermissionRoles[module][key]) {
        PermissionRoles[module][key].forEach((role: string) => {
          roles.add(role);
        });
      }
    });
    return Array.from(roles);
  }

  setupPermissions() {
    Object.keys(PERMISSION_STATE).forEach(module => {
      Object.keys(PERMISSION_STATE[module]).forEach(async context => {
        PERMISSION_STATE[module][context] = await lastValueFrom(
          this.getPermission(module, context),
        );
      });
    });
  }
}

export class PermissionStateInterface {
  [key: string]: {
    create?: boolean;
    read?: boolean;
    update?: boolean;
    active?: boolean;
    delete?: boolean;
    submit?: boolean;
    cancel?: boolean;
  };
}
