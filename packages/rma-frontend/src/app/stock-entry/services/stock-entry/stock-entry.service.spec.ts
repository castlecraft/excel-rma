import { TestBed } from '@angular/core/testing';
import { StockEntryService } from './stock-entry.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('StockEntryService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [],
    }),
  );

  it('should be created', () => {
    const service: StockEntryService = TestBed.get(StockEntryService);
    expect(service).toBeTruthy();
  });
});
