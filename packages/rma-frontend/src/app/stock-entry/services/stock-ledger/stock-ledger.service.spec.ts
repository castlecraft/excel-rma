import { TestBed } from '@angular/core/testing';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { StockLedgerService } from './stock-ledger.service';

describe('StockLedgerService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [],
    }),
  );

  it('should be created', () => {
    const service: StockLedgerService = TestBed.get(StockLedgerService);
    expect(service).toBeTruthy();
  });
});
