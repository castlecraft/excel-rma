import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { from } from 'rxjs';
import {
  DEFAULT_CURRENCY_KEY,
  COUNTRY,
  TIME_ZONE,
  TRANSFER_WAREHOUSE,
  AUTHORIZATION,
  BEARER_TOKEN_PREFIX,
  ACCESS_TOKEN,
  BRAND,
  BACKDATE_PERMISSION,
  BACKDATE_PERMISSION_FOR_DAYS,
  UPDATE_SALES_INVOICE_STOCK,
  UPDATE_PURCHASE_INVOICE_STOCK,
} from './constants/storage';
import { StorageService } from './api/storage/storage.service';
import {
  GET_GLOBAL_DEFAULTS_ENDPOINT,
  RELAY_LIST_NOTE_ENDPOINT,
} from './constants/url-strings';
import { map, switchMap } from 'rxjs/operators';
import { BrandSettings, SettingsService } from './settings/settings.service';

@Injectable()
export class AppService {
  constructor(
    private readonly http: HttpClient,
    private readonly storageService: StorageService,
    private readonly settingsService: SettingsService,
  ) {}

  getGlobalDefault() {
    return this.http.get(GET_GLOBAL_DEFAULTS_ENDPOINT).subscribe({
      next: (success: {
        default_currency: string;
        country: string;
        time_zone: string;
        transferWarehouse: string;
        brand: BrandSettings;
        update_sales_invoice_stock: boolean;
        update_purchase_invoice_stock: boolean;
        backdated_permissions: boolean;
        backdated_permissions_for_days: number;
      }) => {
        if (success?.brand?.faviconURL) {
          this.settingsService.setFavicon(success.brand.faviconURL);
        }
        this.storageService.setItem(
          DEFAULT_CURRENCY_KEY,
          success.default_currency,
        );
        this.storageService.setItem(COUNTRY, success.country);
        this.storageService.setItem(TIME_ZONE, success.time_zone);
        this.storageService.setItem(BRAND, JSON.stringify(success?.brand));
        this.storageService.setItem(
          UPDATE_SALES_INVOICE_STOCK,
          success.update_sales_invoice_stock?.toString(),
        );
        this.storageService.setItem(
          UPDATE_PURCHASE_INVOICE_STOCK,
          success.update_purchase_invoice_stock?.toString(),
        );
        this.storageService.setItem(
          BACKDATE_PERMISSION,
          success.backdated_permissions?.toString(),
        );
        this.storageService.setItem(
          BACKDATE_PERMISSION_FOR_DAYS,
          success.backdated_permissions_for_days?.toString(),
        );
        this.storageService.setItem(
          TRANSFER_WAREHOUSE,
          success.transferWarehouse,
        );
      },
      error: () => {},
    });
  }

  getNoteList(date: string) {
    return this.getHeaders().pipe(
      switchMap(headers => {
        const params = new HttpParams({
          fromObject: {
            filters: `[["expire_notification_on",">","${date}"]]`,
            fields: '["*"]',
          },
        });
        return this.http.get(RELAY_LIST_NOTE_ENDPOINT, { headers, params });
      }),
    );
  }

  getHeaders() {
    return from(this.storageService.getItem(ACCESS_TOKEN)).pipe(
      map(token => ({
        [AUTHORIZATION]: BEARER_TOKEN_PREFIX + token,
      })),
    );
  }
}
