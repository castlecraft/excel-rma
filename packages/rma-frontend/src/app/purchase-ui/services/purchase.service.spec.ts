import { TestBed } from '@angular/core/testing';

import { PurchaseService } from './purchase.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('PurchaseService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [],
    }),
  );

  it('should be created', () => {
    const service: PurchaseService = TestBed.get(PurchaseService);
    expect(service).toBeTruthy();
  });
});
