export const ALL_TERRITORIES = 'All Territories';
export const AUTHORIZATION = 'Authorization';
export const APP_X_WWW_FORM_URLENCODED = 'application/x-www-form-urlencoded';
export const AUTH_SERVER_URL = 'auth_server_url';
export const APP_URL = 'app_url';
export const ACCESS_TOKEN_EXPIRY = 'access_token_expiry';
export const ACCESS_TOKEN = 'access_token';

export const BEARER_TOKEN_PREFIX = 'Bearer ';
export const BACKDATE_PERMISSION = 'backdated_permissions';
export const BASIC_RATE = 'basic_rate';
export const BRAND = 'brand';
export const BACKDATE_PERMISSION_FOR_DAYS = 'backdated_permissions_for_days';

export const COMMUNICATION = 'communication';
export const CLIENT_ID = 'client_id';
export const COMMUNICATION_SERVER = 'communication-server';
export const COMMUNICATION_SERVER_URL = 'communication-server-url';
export const CALLBACK_ENDPOINT = '/callback';
export const COUNTRY = 'country';
export const CONTENT_TYPE = 'Content-Type';
export const CODE_VERIFIER = 'code_verifier';

export const DEFAULT_SELLING_PRICE_LIST = 'default_selling_price_list';
export const DEFAULT_COMPANY = 'defaultCompany';
export const DEFAULT_CURRENCY = 'BDT';
export const DEFAULT_CURRENCY_KEY = 'defaultCurrency';

export const EXPIRES_IN = 'expires_in';

export const HUNDRED_NUMBER_STRING = 100;

export const ISSUER_URL = 'issuer_url';
export const INFRASTRUCTURE_CONSOLE = 'infrastructure-console';
export const IDENTITY_PROVIDER = 'identity-provider';
export const ID_TOKEN = 'id_token';

export const LOGIN_URL = 'login_url';
export const LOGGED_IN = 'logged_in';

export const ONE_HOUR_IN_SECONDS_NUMBER = 3600;

export const PRINT_FORMAT_PREFIX = 'Excel ';
export const POS_PROFILE = 'pos_profile';

export const REDIRECT_URI = 'redirect_uri';
export const REFRESH_TOKEN = 'refresh_token';

export const SERVICES = 'services';
export const STORAGE = 'storage';
export const SCOPE = 'scope';
export const SCOPES_OPENID_ALL = 'openid all';
export const STATE = 'state';
export const SERIAL_NO = 'serial_no';

export const TEN_MINUTES_IN_MS = 600000;
export const TOKEN = 'token';
export const TEN_MINUTES_IN_SECONDS_NUMBER = 600;
export const TWENTY_MINUTES_IN_SECONDS = 1200;
export const TIME_ZONE = 'time_zone';
export const TRANSFER_WAREHOUSE = 'transferWarehouse';

export const UPDATE_SALES_INVOICE_STOCK = 'update_sales_invoice_stock';
export const UPDATE_PURCHASE_INVOICE_STOCK = 'update_purchase_invoice_stock';
export const USER_UUID = 'user_uuid';

export const VALIDATE_SERIAL_BUFFER_COUNT = 50000;

export const WARRANTY_CLAIM = 'warranty-claim';
export const WARRANTY_APP_URL = 'warranty_app_url';

import { DELIVERED_SERIALS_BY } from './app-string';
export const DELIVERED_SERIALS_DISPLAYED_COLUMNS = {
  [DELIVERED_SERIALS_BY.purchase_invoice_name]: [
    'sr_no',
    'serial_no',
    'item_name',
    'warehouse',
    'purchaseWarrantyDate',
    'purchasedOn',
  ],
  [DELIVERED_SERIALS_BY.sales_invoice_name]: [
    'sr_no',
    'serial_no',
    'item_name',
    'warehouse',
    'salesWarrantyDate',
    'soldOn',
  ],
};
