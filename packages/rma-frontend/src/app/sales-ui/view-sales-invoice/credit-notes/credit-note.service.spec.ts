import { TestBed } from '@angular/core/testing';

import { CreditNoteService } from './credit-note.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CreditNoteService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [],
    }),
  );

  it('should be created', () => {
    const service: CreditNoteService = TestBed.get(CreditNoteService);
    expect(service).toBeTruthy();
  });
});
