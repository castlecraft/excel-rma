import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject, of } from 'rxjs';

export interface Warranty {
  claimNo: string;
  claimedItem: string;
  serial: string;
  claimsReceivedDate: string;
  status: string;
  deliveryDate: string;
  createdBy: string;
}

export class InvoiceWarrantyDataSource extends DataSource<Warranty> {
  itemSubject = new BehaviorSubject<Warranty[]>([]);

  constructor() {
    super();
  }

  connect() {
    return this.itemSubject.asObservable();
  }
  disconnect() {
    this.itemSubject.complete();
  }

  loadItems() {
    this.getWarrantyList().subscribe(warranty =>
      this.itemSubject.next(warranty),
    );
  }

  data() {
    return this.itemSubject.value;
  }

  update(data) {
    this.itemSubject.next(data);
  }

  getWarrantyList() {
    return of([]);
  }
}
