import { TestBed } from '@angular/core/testing';

import { SalesReturnService } from './sales-return.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('SalesReturnService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [],
    }),
  );

  it('should be created', () => {
    const service: SalesReturnService = TestBed.get(SalesReturnService);
    expect(service).toBeTruthy();
  });
});
