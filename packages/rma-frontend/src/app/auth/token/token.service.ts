import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, from, of, throwError } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import {
  ACCESS_TOKEN,
  CALLBACK_ENDPOINT,
  CONTENT_TYPE,
  EXPIRES_IN,
  SCOPES_OPENID_ALL,
  TOKEN,
  APP_X_WWW_FORM_URLENCODED,
  CODE_VERIFIER,
  ID_TOKEN,
  LOGGED_IN,
  ONE_HOUR_IN_SECONDS_NUMBER,
  REFRESH_TOKEN,
  STATE,
  TEN_MINUTES_IN_SECONDS_NUMBER,
  BEARER_TOKEN_PREFIX,
  APP_URL,
  AUTH_SERVER_URL,
  BACKDATE_PERMISSION,
  BACKDATE_PERMISSION_FOR_DAYS,
  DEFAULT_COMPANY,
  DEFAULT_SELLING_PRICE_LIST,
  WARRANTY_APP_URL,
} from '../../constants/storage';
import { API_INFO_ENDPOINT } from '../../constants/url-strings';
import { switchMap } from 'rxjs/operators';
import { StorageService } from '../../api/storage/storage.service';
import { BearerToken } from './bearer-token.interface';
import * as sjcl from 'sjcl';
import { Browser } from '@capacitor/browser';
import {
  InvalidAccessTokenError,
  InvalidExpirationError,
} from './token.errors';

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  private iab = Browser;
  config: Observable<any>;
  private headers = {
    [CONTENT_TYPE]: APP_X_WWW_FORM_URLENCODED,
  };

  initialize(): void {
    this.config = this.getMessage().pipe(
      switchMap(response => {
        this.setInfoLocalStorage(response);
        return of({
          authServerUrl: response.authorizationURL,
          clientId: response.frontendClientId,
          redirectUri: response.appURL + CALLBACK_ENDPOINT,
          responseType: TOKEN,
          scope: SCOPES_OPENID_ALL,
          profileUrl: response.profileURL,
          tokenUrl: response.tokenURL,
          revocationUrl: response.revocationURL,
          callbackProtocol: response.callbackProtocol,
        });
      }),
    );
  }

  constructor(
    private readonly http: HttpClient,
    private readonly storageService: StorageService,
  ) {
    this.initialize();
  }

  getMessage(): Observable<any> {
    return this.http.get<any>(API_INFO_ENDPOINT);
  }

  loadProfile() {
    return this.config.pipe(
      switchMap(config => {
        return this.getToken().pipe(
          switchMap(token => {
            return this.http.get<any>(config.profileUrl, {
              headers: { authorization: BEARER_TOKEN_PREFIX + token },
            });
          }),
        );
      }),
    );
  }

  getToken() {
    return forkJoin({
      expiration: from(this.storageService.getItem(EXPIRES_IN)),
      accessToken: from(this.storageService.getItem(ACCESS_TOKEN)),
    }).pipe(
      switchMap(({ expiration, accessToken }) => {
        if (['undefined', 'null'].includes(accessToken) || !accessToken) {
          return throwError(() => new InvalidAccessTokenError());
        }

        if (!expiration) {
          return throwError(() => new InvalidExpirationError());
        }

        const now = new Date();
        const expirationTime = new Date(expiration);

        // expire 10 min early
        expirationTime.setSeconds(
          expirationTime.getSeconds() - TEN_MINUTES_IN_SECONDS_NUMBER,
        );
        if (now < expirationTime) {
          return of(accessToken);
        }
        return this.refreshToken();
      }),
    );
  }

  getRefreshToken(): Promise<string> {
    return this.storageService.getItem(REFRESH_TOKEN);
  }

  refreshToken() {
    return this.config.pipe(
      switchMap(config => {
        return from(this.getRefreshToken()).pipe(
          switchMap(refreshToken => {
            const requestBody = {
              grant_type: 'refresh_token',
              refresh_token: refreshToken,
              redirect_uri: config.redirectUri,
              client_id: config.clientId,
              scope: config.scope,
            };
            return this.http
              .post<BearerToken>(
                config.tokenUrl,
                new URLSearchParams(requestBody).toString(),
                {
                  headers: this.headers,
                },
              )
              .pipe(
                switchMap(bearerToken => {
                  this.revokeToken();
                  const expirationTime = new Date();
                  const expiresIn =
                    bearerToken.expires_in || ONE_HOUR_IN_SECONDS_NUMBER;
                  expirationTime.setSeconds(
                    expirationTime.getSeconds() + Number(expiresIn),
                  );
                  this.storageService.setItem(
                    EXPIRES_IN,
                    expirationTime.toISOString(),
                  );
                  this.storageService.setItem(ID_TOKEN, bearerToken.id_token);
                  this.storageService.setItem(
                    ACCESS_TOKEN,
                    bearerToken.access_token,
                  );
                  this.storageService.setItem(
                    REFRESH_TOKEN,
                    bearerToken.refresh_token,
                  );
                  return of(bearerToken.access_token);
                }),
              );
          }),
        );
      }),
    );
  }

  revokeToken(refresh: boolean = false) {
    forkJoin({
      config: this.config,
      token: from(this.storageService.getItem(ACCESS_TOKEN)),
    })
      .pipe(
        switchMap(({ config, token }) => {
          return this.http.post(
            config.revocationUrl,
            new URLSearchParams({ token }).toString(),
            {
              headers: { ...this.headers },
            },
          );
        }),
      )
      .subscribe({
        next: async () => {
          if (refresh) {
            await this.storageService.setItem(LOGGED_IN, 'false');
          }
        },
        error: () => {},
      });
  }

  processCode(url: string) {
    const urlParts = new URL(url);
    const query = new URLSearchParams(urlParts.searchParams);
    const code = query.get('code') as string;
    if (!code) {
      return;
    }

    const error = query.get('error');
    if (error) {
      return;
    }

    const state = query.get('state') as string;
    forkJoin({
      savedState: from(this.storageService.getItem(STATE)),
      codeVerifier: from(this.storageService.getItem(CODE_VERIFIER)),
      config: this.config,
    })
      .pipe(
        switchMap(({ savedState, codeVerifier, config }) => {
          if (savedState !== state) {
            return of({ ErrorInvalidState: true });
          }
          const req = {
            grant_type: 'authorization_code',
            code,
            redirect_uri: config.redirectUri,
            client_id: config.clientId,
            scope: config.scope,
            code_verifier: codeVerifier,
          };

          return this.http.post<BearerToken>(
            config.tokenUrl,
            new URLSearchParams(req).toString(),
            {
              headers: this.headers,
            },
          );
        }),
      )
      .subscribe({
        next: async (response: BearerToken) => {
          const expiresIn = response.expires_in || ONE_HOUR_IN_SECONDS_NUMBER;
          const expirationTime = new Date();
          expirationTime.setSeconds(
            expirationTime.getSeconds() + Number(expiresIn),
          );

          await this.storageService.setItem(
            ACCESS_TOKEN,
            response.access_token,
          );
          await this.storageService.setItem(
            REFRESH_TOKEN,
            response.refresh_token,
          );
          await this.storageService.setItem(
            EXPIRES_IN,
            expirationTime.toISOString(),
          );
          await this.storageService.setItem(ID_TOKEN, response.id_token);
          await this.storageService.setItem(LOGGED_IN, 'true');

          await this.storageService.removeItem(STATE);
          await this.storageService.removeItem(CODE_VERIFIER);
        },
        error: err => {
          // eslint-disable-next-line
          console.error({
            timestamp: new Date().toISOString(),
            ...err,
          });
        },
      });
  }

  logIn() {
    this.generateAuthUrl().subscribe({
      next: url => {
        this.iab.open({ url, windowName: '_self' });
      },
    });
  }

  async logOut() {
    this.revokeToken(true);
    this.storageService.getItem(AUTH_SERVER_URL).then(url => {
      window.location.href = `${url}/api/method/frappe.handler.web_logout`;
    });
    this.storageService.clear();
    await this.storageService.setItem(LOGGED_IN, 'false');
  }

  generateAuthUrl() {
    return this.config.pipe(
      switchMap(config => {
        const state = this.generateRandomString();
        this.storageService.setItem(STATE, state);

        const codeVerifier = this.generateRandomString();
        this.storageService.setItem(CODE_VERIFIER, codeVerifier);

        const challenge = sjcl.codec.base64
          .fromBits(sjcl.hash.sha256.hash(codeVerifier))
          .replace(/\+/g, '-')
          .replace(/\//g, '_')
          .replace(/=/g, '');

        let url = config.authServerUrl;
        url += '?scope=' + encodeURIComponent(config.scope);
        url += '&response_type=code';
        url += '&client_id=' + config.clientId;
        url += '&redirect_uri=' + encodeURIComponent(config.redirectUri);
        url += '&state=' + state;
        url += '&code_challenge_method=S256&prompt=select_account';
        url += '&code_challenge=' + challenge;

        return of(url);
      }),
    );
  }

  generateRandomString(stateLength: number = 32) {
    let result = '';
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < stateLength; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  async setInfoLocalStorage(response: any) {
    await this.storageService.setItem(APP_URL, response.appURL);
    await this.storageService.setItem(
      WARRANTY_APP_URL,
      response.warrantyAppURL,
    );
    await this.storageService.setItem(AUTH_SERVER_URL, response.authServerURL);
    this.storageService.getItem(ACCESS_TOKEN).then(async access_token => {
      if (!access_token) {
        await this.storageService.setItem(LOGGED_IN, 'false');
      }
    });
    await this.storageService.setItem(DEFAULT_COMPANY, response.defaultCompany);
    await this.storageService.setItem(
      DEFAULT_SELLING_PRICE_LIST,
      response.sellingPriceList,
    );
    await this.storageService.setItem(
      BACKDATE_PERMISSION,
      response.backdated_permissions,
    );
    await this.storageService.setItem(
      BACKDATE_PERMISSION_FOR_DAYS,
      response.backdated_permissions_for_days,
    );
  }
}
