export const INVALID_EXPIRATION = 'Invalid Expiration';
export const INVALID_ACCESS_TOKEN = 'Invalid Access Token';
export class InvalidExpirationError extends Error {
  constructor() {
    super(INVALID_EXPIRATION);
  }
}
export class InvalidAccessTokenError extends Error {
  constructor() {
    super(INVALID_ACCESS_TOKEN);
  }
}
