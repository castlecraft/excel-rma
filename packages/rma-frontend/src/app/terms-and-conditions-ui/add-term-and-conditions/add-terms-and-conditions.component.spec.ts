import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material/dialog';
import { IonicModule } from '@ionic/angular';

import { AddTermsAndConditionsComponent } from './add-terms-and-conditions.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('AddTermsAndConditionsComponent', () => {
  let component: AddTermsAndConditionsComponent;
  let fixture: ComponentFixture<AddTermsAndConditionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddTermsAndConditionsComponent],
      imports: [IonicModule.forRoot(), HttpClientTestingModule],
      providers: [{ provide: MatDialogRef, useValue: {} }],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(AddTermsAndConditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
