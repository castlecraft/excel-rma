import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { TermsAndConditionsService } from '../services/TermsAndConditions/terms-and-conditions.service';
import { TermsAndConditionsDataSource } from './terms-and-conditions-datasource';
import { FormControl } from '@angular/forms';
import {
  Observable,
  debounceTime,
  distinctUntilChanged,
  map,
  startWith,
  switchMap,
} from 'rxjs';
import { ValidateInputSelected } from '../../common/pipes/validators';
import { PopoverController } from '@ionic/angular';
import { ConfirmDialogComponent } from '../../common/components/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { AddTermsAndConditionsComponent } from '../add-term-and-conditions/add-terms-and-conditions.component';

@Component({
  selector: 'app-terms-and-conditions',
  templateUrl: './terms-and-conditions.page.html',
  styleUrls: ['./terms-and-conditions.page.scss'],
})
export class TermsAndConditionsPage implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: TermsAndConditionsDataSource;
  displayedColumns = ['sr_no', 'terms_and_conditions_name', 'delete'];
  tnc = new FormControl();
  filteredTncList: Observable<any[]>;
  validateInput: any = ValidateInputSelected;

  constructor(
    private readonly location: Location,
    private readonly termsAndConditionsService: TermsAndConditionsService,
    private readonly popoverController: PopoverController,
    private readonly route: ActivatedRoute,
    private readonly dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(() => {
      this.paginator.firstPage();
    });
    this.dataSource = new TermsAndConditionsDataSource(
      this.termsAndConditionsService,
    );
    this.dataSource.loadItems();

    this.filteredTncList = this.tnc.valueChanges.pipe(
      startWith(''),
      distinctUntilChanged(),
      debounceTime(1000),
      switchMap(value => {
        return this.termsAndConditionsService
          .getTermsAndConditionsList(value)
          .pipe(map(res => res.docs));
      }),
    );
  }

  navigateBack() {
    this.location.back();
  }

  async addTermsAndConditions() {
    const popover = await this.popoverController.create({
      component: AddTermsAndConditionsComponent,
      componentProps: {
        passedFrom: 'add',
      },
    });

    await popover.present();
    popover.onDidDismiss().then(res => {
      if (res?.data?.success)
        this.dataSource.loadItems(
          undefined,
          this.sort.direction,
          this.paginator.pageIndex,
          this.paginator.pageSize,
        );
    });
  }

  async updateTermsAndConditions(uuid: string) {
    const popover = await this.popoverController.create({
      component: AddTermsAndConditionsComponent,
      componentProps: {
        passedFrom: 'update',
        uuid,
      },
    });

    await popover.present();
    popover.onDidDismiss().then(res => {
      if (res.data.success)
        this.dataSource.loadItems(
          this.tnc.value,
          this.sort.direction,
          this.paginator.pageIndex,
          this.paginator.pageSize,
        );
    });
  }

  confirmDelete(uuid: string) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        message: 'Are you sure you want to delete?',
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteTermsAndConditions(uuid);
      }
    });
  }

  deleteTermsAndConditions(uuid: string) {
    this.termsAndConditionsService.deleteTermsAndConditions(uuid).subscribe({
      next: () => {
        this.dataSource.loadItems(
          this.tnc.value,
          this.sort.direction,
          this.paginator.pageIndex,
          this.paginator.pageSize,
        );
      },
    });
  }

  setFilter(event?) {
    this.dataSource.loadItems(
      this.tnc.value,
      this.sort.direction,
      event?.pageIndex || 0,
      event?.pageSize || 30,
    );
  }

  clearFilters() {
    this.tnc.reset();
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 30;
    this.dataSource.loadItems(
      undefined,
      undefined,
      this.paginator.pageIndex,
      this.paginator.pageSize,
    );
  }
}
