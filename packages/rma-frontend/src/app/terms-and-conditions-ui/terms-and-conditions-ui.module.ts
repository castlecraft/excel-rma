import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TermsAndConditionsPageModule } from './terms-and-conditions/terms-and-conditions.module';
import { AddTermsAndConditionsComponent } from './add-term-and-conditions/add-terms-and-conditions.component';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AddTermsAndConditionsComponent],
  imports: [CommonModule, MaterialModule, FormsModule, ReactiveFormsModule],
  exports: [TermsAndConditionsPageModule, AddTermsAndConditionsComponent],
})
export class TermsAndConditionsUiModule {}
