import { Component, OnInit } from '@angular/core';
import { AUTH_SERVER_URL, LOGGED_IN } from '../constants/storage';
import { SET_ITEM, StorageService } from '../api/storage/storage.service';
import { AppService } from '../app.service';
import { TokenService } from '../auth/token/token.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  loggedIn: boolean = false;
  picture: string;
  state: string;
  email: string;
  name: string;
  spinner = true;
  noteList: NoteInterface[] = [];

  constructor(
    private readonly storageService: StorageService,
    private readonly appService: AppService,
    private readonly tokenService: TokenService,
  ) {}

  ngOnInit() {
    this.storageService.getItem(LOGGED_IN).then(loggedIn => {
      this.loggedIn = loggedIn === 'true';
      if (this.loggedIn) {
        this.loadProfile();
        this.appService.getGlobalDefault();
        this.loadNoteList();
      }
      this.spinner = false;
    });

    this.storageService.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          this.loggedIn = res.value?.value === 'true';
          if (this.loggedIn) {
            this.loadProfile();
            this.loadNoteList();
          }
        }
      },
      error: () => {},
    });
  }

  loadProfile() {
    this.tokenService.loadProfile().subscribe({
      next: profile => {
        this.email = profile.email;
        this.name = profile.name;
        this.spinner = false;
      },
      error: () => {},
    });
  }

  async loadNoteList() {
    const currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 1);
    const date = [
      currentDate.getDate(),
      currentDate.getMonth() + 1,
      currentDate.getFullYear(),
    ].join('-');

    await this.storageService.getItem(AUTH_SERVER_URL).then(app_url => {
      this.appService.getNoteList(date).subscribe({
        next: (response: { data: any[] }) => {
          response.data.forEach(data => {
            data.content = data.content.replace('src="', 'src="' + app_url);
          });
          this.noteList = response.data;
        },
        error: () => {},
      });
    });
  }
}

export interface NoteInterface {
  name: string;
  owner: string;
  creation: string;
  title: string;
  notify_on_login: number;
  notify_on_every_login: number;
  expire_notification_on: string;
  content: string;
}
