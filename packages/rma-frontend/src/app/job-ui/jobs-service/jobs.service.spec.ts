import { TestBed } from '@angular/core/testing';

import { JobsService } from './jobs.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('JobsService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [],
    }),
  );

  it('should be created', () => {
    const service: JobsService = TestBed.get(JobsService);
    expect(service).toBeTruthy();
  });
});
