import { Component, OnInit } from '@angular/core';
import {
  ALL_TERRITORIES,
  LOGGED_IN,
  WARRANTY_APP_URL,
} from './constants/storage';
import { AppService } from './app.service';
import { PERMISSION_STATE } from './constants/permission-roles';
import { SET_ITEM, StorageService } from './api/storage/storage.service';
import { TokenService } from './auth/token/token.service';
import { Router } from '@angular/router';
import { PermissionManager } from './api/permission/permission.service';
import {
  SYSTEM_MANAGER,
  TERRITORY,
  USER_ROLE,
  WAREHOUSES,
} from './constants/app-string';
import { SettingsService } from './settings/settings.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
})
export class AppComponent implements OnInit {
  loggedIn: boolean = false;
  permissionState: any = PERMISSION_STATE;

  isSettingMenuVisible: boolean = false;
  isSalesMenuVisible: boolean = false;
  isStockMenuVisible: boolean = false;
  isRelayMenuVisible: boolean = false;
  isSystemManager: boolean = false;

  name: string = '';
  imageURL: string = '';

  constructor(
    private readonly appService: AppService,
    private readonly storageService: StorageService,
    private readonly tokenService: TokenService,
    private readonly router: Router,
    private readonly permissionManager: PermissionManager,
    private readonly settingService: SettingsService,
  ) {}

  ngOnInit() {
    this.storageService.getItem(LOGGED_IN).then(loggedIn => {
      loggedIn === 'true' ? (this.loggedIn = true) : (this.loggedIn = false);
      if (this.loggedIn) {
        this.loadProfile();
        this.appService.getGlobalDefault();
        this.getRoles();
      }
    });

    this.storageService.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          res.value?.value === 'true'
            ? (this.loggedIn = true)
            : (this.loggedIn = false);

          if (!this.loggedIn) {
            this.router.navigate(['/home']).then(() => {});
          }
          if (this.loggedIn) {
            this.loadProfile();
            this.appService.getGlobalDefault();
            this.getRoles();
          }
        }
      },
      error: () => {},
    });
  }

  login() {
    this.tokenService.logIn();
  }

  logout() {
    this.tokenService.logOut();
  }

  loadProfile() {
    this.tokenService.loadProfile().subscribe({
      next: profile => {
        this.loggedIn = true;
        this.name = profile.name;
        this.imageURL = profile.picture;
      },
      error: () => {},
    });
  }

  getRoles() {
    this.settingService.getRoles().subscribe({
      next: async (res: {
        roles: string[];
        warehouse: string[];
        territory: string[];
      }) => {
        this.loggedIn = true;
        if (res) {
          if (res.roles.find(x => x === SYSTEM_MANAGER)) {
            this.isSystemManager = true;
          }
          await this.storageService.setItem(
            USER_ROLE,
            res.roles?.toString() || '',
          );
          await this.storageService.setItem(
            WAREHOUSES,
            res?.warehouse?.toString() || '',
          );
          const filtered_territory = res?.territory?.filter(
            territory => territory !== ALL_TERRITORIES,
          );
          this.storageService.setItem(
            TERRITORY,
            filtered_territory?.toString() || '',
          );
          this.permissionManager.setupPermissions();
        }
      },
      error: () => {
        this.logout();
      },
    });
  }

  openWarrantyApp() {
    this.storageService.getItem(WARRANTY_APP_URL).then(warrantyUrl => {
      window.open(warrantyUrl, '_blank');
    });
  }

  openSerialSearch() {
    this.storageService.getItem(WARRANTY_APP_URL).then(warrantyUrl => {
      window.open(`${warrantyUrl}/serial-search`, '_blank');
    });
  }
}
