import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AppService } from './app.service';
import { SalesUiModule } from './sales-ui/sales-ui.module';
import { PurchaseUiModule } from './purchase-ui/purchase-ui.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpErrorHandler } from './common/interfaces/services/http-error-handler/http-error-handler.service';
import { MessageService } from './common/interfaces/services/message/message.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StorageService } from './api/storage/storage.service';
import { AppCommonModule } from './common/app-common.module';
import { TimeService } from './api/time/time.service';
import { CsvJsonService } from './api/csv-json/csv-json.service';
import { ProblemUiModule } from './problem-ui/problem-ui.module';
import { JobUIModule } from './job-ui/job-ui.module';
import { PermissionManager } from './api/permission/permission.service';
import { ApiModule } from './api/api.module';
import { CommonComponentModule } from './common/components/common-component.module';
import { TermsAndConditionsUiModule } from './terms-and-conditions-ui/terms-and-conditions-ui.module';
import { TokenService } from './auth/token/token.service';
import { RefreshTokenInterceptor } from './auth/refresh-token.interceptor';

@NgModule({
  declarations: [AppComponent],
  imports: [
    CommonComponentModule,
    JobUIModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    SalesUiModule,
    PurchaseUiModule,
    ApiModule,
    BrowserAnimationsModule,
    AppCommonModule,
    ProblemUiModule,
    TermsAndConditionsUiModule,
  ],
  providers: [
    AppService,
    TokenService,
    StatusBar,
    MessageService,
    TimeService,
    CsvJsonService,
    HttpErrorHandler,
    SplashScreen,
    PermissionManager,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RefreshTokenInterceptor,
      multi: true,
    },
    StorageService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
