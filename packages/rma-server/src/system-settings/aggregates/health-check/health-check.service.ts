import {
  HealthIndicatorFunction,
  TypeOrmHealthIndicator,
} from '@nestjs/terminus';
import { Injectable } from '@nestjs/common';
import { DataSource } from 'typeorm';
import { InjectDataSource } from '@nestjs/typeorm';
import { DEFAULT } from '../../../constants/typeorm.connection';

export const HEALTH_ENDPOINT = '/api/healthz';

@Injectable()
export class HealthCheckAggregateService {
  constructor(
    @InjectDataSource(DEFAULT)
    private readonly typeormConnection: DataSource,
    private readonly database: TypeOrmHealthIndicator,
  ) {}

  createTerminusOptions(): HealthIndicatorFunction[] {
    const healthEndpoint: HealthIndicatorFunction[] = [
      async () =>
        this.database.pingCheck('database', {
          connection: this.typeormConnection,
        }),
    ];
    return healthEndpoint;
  }
}
