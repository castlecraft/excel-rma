import { Provider } from '@nestjs/common';

import {
  ConfigService,
  MONGO_URI_PREFIX,
  DB_USER,
  DB_PASSWORD,
  DB_HOST,
  DB_NAME,
} from '../../config/config.service';
import { Agenda } from 'agenda';

export const AGENDA_TOKEN = 'AgendaProvider';
export const MAJORITY = 'majority';

export const AgendaProvider: Provider = {
  provide: AGENDA_TOKEN,
  useFactory: async (config: ConfigService) => {
    const mongoUriPrefix = config.get(MONGO_URI_PREFIX) || 'mongodb';
    const mongoOptions = 'retryWrites=true';
    const agenda = new Agenda({
      db: {
        address: `${mongoUriPrefix}://${config.get(DB_USER)}:${config.get(
          DB_PASSWORD,
        )}@${config.get(DB_HOST)}/${config.get(DB_NAME)}?${mongoOptions}`,
      },
    });
    await agenda.start();
    return agenda;
  },
  inject: [ConfigService],
};
