import { BaseEntity, Entity, ObjectIdColumn, ObjectId, Column } from 'typeorm';

@Entity()
export class TermsAndConditions extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectId;

  @Column()
  uuid: string;

  @Column()
  terms_and_conditions: string;
}
